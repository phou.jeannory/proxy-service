package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.net.InetSocketAddress;
import java.net.Proxy;

@EnableDiscoveryClient
@EnableZuulProxy
@SpringBootApplication
public class ProxyServiceApplication {

	//pour tester http://localhost:9999/user-service/api/test/users
	//host + service + api
	public static void main(String[] args) {
		SpringApplication.run(ProxyServiceApplication.class, args);
	}

}
